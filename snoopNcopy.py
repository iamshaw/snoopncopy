import win32file
import shutil
import os.path
import errno
import time
from datetime import datetime


DESTINATION = "E:\\TEMP"
BACKUP_DIRNAME_FORMAT = "%Y_%m_%d__%H_%M_%S"
# LOG = "E:\\TEMP\logs"
REFRESH_TIME_IN_MINUTES = 0.1       # Set as 5 minutes
RUNTIME_IN_MINUTES = 1              # Set as 120 or 90 minutes


def locate_usb():
    # import win32file
    drive_list = []
    drive_bits = win32file.GetLogicalDrives()
    for d in range(1, 26):
        mask = 1 << d
        if drive_bits & mask:
            # here if the drive is at least there
            dirname = '%c:\\' % chr(ord('A')+d)
            t = win32file.GetDriveType(dirname)
            if t == win32file.DRIVE_REMOVABLE:
                drive_list.append(dirname)
    return drive_list


def copy_dir(src, dest):
    # import shutil
    # import errno
    print("Trying to copy contents...")
    try:
        shutil.copytree(src, dest, ignore=shutil.ignore_patterns('*.ink', '*.exe'))
    except OSError as e:
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
            print("Copied")
        else:
            return e


if __name__ == "__main__":
    # import time
    START_TIME = time.time()
    drive_found = False
    copy_error = False
    while True:
        TIME_ELAPSED = time.time() - START_TIME
        if TIME_ELAPSED >= (RUNTIME_IN_MINUTES * 60):
            exit()
        drives = locate_usb()
        print("Number of USB devices found: %d" % len(drives))
        if len(drives) > 0:
            drive_found = True
        else:
            drive_found = False

        if drive_found is False:
            print("No drive found... Sleeping")
            time.sleep(REFRESH_TIME_IN_MINUTES * 60)
            continue

        for i in drives:
            print(i)
            # import os.path
            # from datetime import datetime
            # print(os.listdir(i))
            # for j in os.walk(i):
            #     print("  %s" % str(j))
            # backup_time = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
            # for i in str(backup_time).split(":"):
            #     temp = int(i).__round__(2)
            #     os.path.join(DESTN, str(temp))
            error_msg = copy_dir(i, os.path.join(DESTINATION, datetime.now().strftime(BACKUP_DIRNAME_FORMAT)))
            if error_msg:
                print(error_msg)
                copy_error = True
            else:
                copy_error = False

            if copy_error is False:
                exit()
        time.sleep(REFRESH_TIME_IN_MINUTES * 60)
